package com.example.demo.qut;

import lombok.extern.slf4j.Slf4j;
import mqtt.annotation.Topic;
import mqtt.receiver.MqttMessage;
import mqtt.receiver.Receiver;
import org.springframework.stereotype.Component;

/**
 * 链接消息
 *
 */
@Slf4j
@Topic(name = "$SYS/brokers/+/clients/+/connected")
@Component
public class ConnectReceiver implements Receiver {


	@Override
	public void handleMessage(MqttMessage mqttMessage) {
		log.info("设备链接：{}",mqttMessage.getTopic());
	}
}
